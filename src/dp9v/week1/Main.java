package dp9v.week1;


/**
 * Created by dpolkovnikov on 13.02.17.
 */
public class Main {
	public static void main(String[] args) {
		Printer.printTime();

		Thread generator = new Thread(new Generator());
		Thread timer = new Thread(new Timer());
		generator.setDaemon(true);
		timer.setDaemon(true);
		Thread printer = new Thread(new Printer());
		timer.start();
		printer.start();
		generator.start();
	}
}
