package dp9v.week1;

/**
 * Created by dpolkovnikov on 13.02.17.
 */
public class Timer implements Runnable {
	public static final Object monitor = new Object();

	@Override
	public void run() {
		while(true){
			try {
				Thread.sleep(1000);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			synchronized (monitor){
				monitor.notifyAll();
			}
		}
	}
}
