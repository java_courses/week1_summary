package dp9v.week1;

import com.sun.scenario.effect.impl.prism.PrImage;

import java.util.Hashtable;
import java.util.Random;

/**
 * Created by dpolkovnikov on 13.02.17.
 */
public class Generator implements Runnable {
	private static final Integer                     MAX_NUMBER = 10;
	private static       Hashtable<Integer, Integer> numbers    = new Hashtable<>(MAX_NUMBER);

	public static Hashtable<Integer, Integer> getNumbers() {
		return numbers;
	}

	@Override
	public void run() {
		Random randomizer = new Random();
		while(true) {
			Integer newNumber = randomizer.nextInt(MAX_NUMBER);
			synchronized (Timer.monitor) {
				if(numbers.containsKey(newNumber))
					numbers.put(newNumber, numbers.get(newNumber) + 1);
				else
					numbers.put(newNumber, 1);

				try {
					Timer.monitor.wait();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
