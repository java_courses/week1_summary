package dp9v.week1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by dpolkovnikov on 13.02.17.
 */
public class Printer implements Runnable {
	private static final Integer MAX_REPEATS   = 5;
	private              int     printInterval = 5;
	private              int     repeats       = 1;

	public Printer() {
		printInterval = 5;
	}

	public Printer(int printInterval) {

		this.printInterval = printInterval;
	}

	public static void printTime() {
		Date       start     = new Date(System.currentTimeMillis());
		DateFormat formatter = new SimpleDateFormat("hh:mm:ss");
		String     s         = formatter.format(start);
		System.out.println(s + ": ");
	}

	@Override
	public void run() {
		boolean stopMark = false;
		while(!stopMark) {
			synchronized (Timer.monitor) {
				try {
					Timer.monitor.wait();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}

				StringBuilder sb = new StringBuilder();
				for(Map.Entry<Integer, Integer> number :
						Generator.getNumbers().entrySet()) {
					sb.append(number.getKey() + ": " + number.getValue() + "\n");
					if(number.getValue() > MAX_REPEATS)
						stopMark = true;
				}
				if(repeats >= printInterval || stopMark) {
					printTime();
					System.out.println(sb.toString());
					repeats = 0;
					if(stopMark)
						System.out.println("Найдено переполнение счетчика");

				}
				repeats++;
			}
		}
	}

}
